<?php
/**
 * Created by PhpStorm.
 * User: luisfernando
 * Date: 16/04/2017
 * Time: 18:58
 */
namespace Api\Model;
use \Illuminate\Database\Eloquent\Model as Illuminate;


class User extends Illuminate
{
    protected $table = 'user';

    public $fillable = ['registration', 'name', 'password'];
}