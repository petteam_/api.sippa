<?php
/**
 * Created by PhpStorm.
 * User: luisfernando
 * Date: 16/04/17
 * Time: 22:09
 */

namespace Api\Model;
use \Illuminate\Database\Eloquent\Model as Illuminate;



class Discipline extends Illuminate
{
    protected  $table = 'discipline';

    public $fillable = ['code', 'workload', 'name','teacher' ,'semester'];

}