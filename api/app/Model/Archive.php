<?php
/**
 * Created by PhpStorm.
 * User: luisfernando
 * Date: 19/04/17
 * Time: 19:40
 */

namespace Api\Model;
use \Illuminate\Database\Eloquent\Model as Illuminate;


class Archive extends Illuminate
{
    protected $table = 'archive';

    public $fillable = ['discipline_id', 'name', 'url'];
}