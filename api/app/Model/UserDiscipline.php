<?php
/**
 * Created by PhpStorm.
 * User: luisfernando
 * Date: 16/04/17
 * Time: 22:13
 */

namespace Api\Model;
use \Illuminate\Database\Eloquent\Model as Illuminate;

class UserDiscipline extends Illuminate
{
    protected $table = 'user_discipline';

    public $fillable = ['user_id', 'discipline_id'];

    public $timestamps = false;
}